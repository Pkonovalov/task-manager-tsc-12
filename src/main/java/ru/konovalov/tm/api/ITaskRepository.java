package ru.konovalov.tm.api;

import ru.konovalov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {
    List<Task> findALL();

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task removeOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByName(String name);

}
