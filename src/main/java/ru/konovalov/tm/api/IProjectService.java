package ru.konovalov.tm.api;

import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.model.Project;

import java.util.List;

public interface IProjectService {
    List<Project> findAll();

    void add(Project project);

    void remove(Project project);

    void clear();

    Object add(String name, String description);

    Project removeOneById(String id);

    Project findOneById(String id);

    Project findOneByName(String name);

    Project removeOneByName(String name);

    Project removeProjectByIndex(Integer index);

    Project findOneByIndex(Integer index);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project updateProjectById(String id, String name, String description);

    Project startProjectById(String id);

    Project startProjectByIndex(Integer index);

    Project startProjectByName(String name);

    Project finishProjectById(String id);

    Project finishProjectByIndex(Integer index);

    Project finishProjectByName(String name);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByName(String name, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
